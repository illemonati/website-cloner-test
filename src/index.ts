import scrape from "website-scraper";
import PuppeteerPlugin from "website-scraper-puppeteer";
import path from "path";

scrape({
    urls: ["https://www.qoom.io/"],
    directory: path.resolve(__dirname, "../outputs/qoomio"),
    recursive: true,
    maxRecursiveDepth: 1000,
    plugins: [
        new PuppeteerPlugin({
            launchOptions: {
                headless: true,
            },
            scrollToBottom: {
                timeout: 10000,
                viewportN: 10,
            },
        }),
    ],
});
