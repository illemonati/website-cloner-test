"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const website_scraper_1 = __importDefault(require("website-scraper"));
const website_scraper_puppeteer_1 = __importDefault(require("website-scraper-puppeteer"));
const path_1 = __importDefault(require("path"));
website_scraper_1.default({
    urls: ["https://www.qoom.io/"],
    directory: path_1.default.resolve(__dirname, "../outputs/qoomio"),
    recursive: true,
    maxRecursiveDepth: 1000,
    plugins: [
        new website_scraper_puppeteer_1.default({
            launchOptions: {
                headless: true,
            },
            scrollToBottom: {
                timeout: 10000,
                viewportN: 10,
            },
        }),
    ],
});
